/**
 *
 * @module fun-index
 */
;(function () {
  'use strict'

  /* imports */
  var guarded = require('guarded')
  var funAssert = require('fun-assert')

  var isNumber = funAssert.type('Number')
  var isArrayOfNumber = funAssert.type('[Number]')

  /* exports */
  module.exports = guarded({
    inputs: [isNumber],
    f: index,
    output: isArrayOfNumber
  })

  /**
   *
   * @function module:fun-index.index
   *
   * @param {Number} length - of the index array to generate
   *
   * @return {[Number]} an array of length specified, starting at 0
   */
  function index (length) {
    return Array.apply(null, { length: length }).map(function (x, i) {
      return i
    })
  }
})()

