# [fun-index](https://bagrounds.gitlab.io/fun-index)

Generate an index array [0, 1, 2, ...]

[![build-status](https://gitlab.com/bagrounds/fun-index/badges/master/build.svg)](https://gitlab.com/bagrounds/fun-index/commits/master)
[![coverage-report](https://gitlab.com/bagrounds/fun-index/badges/master/coverage.svg)](https://gitlab.com/bagrounds/fun-index/commits/master)
[![license](https://img.shields.io/npm/l/fun-index.svg)](https://www.npmjs.com/package/fun-index)
[![version](https://img.shields.io/npm/v/fun-index.svg)](https://www.npmjs.com/package/fun-index)
[![downloads](https://img.shields.io/npm/dt/fun-index.svg)](https://www.npmjs.com/package/fun-index)
[![downloads-monthly](https://img.shields.io/npm/dm/fun-index.svg)](https://www.npmjs.com/package/fun-index)
[![dependencies](https://david-dm.org/bagrounds/fun-index/status.svg)](https://david-dm.org/bagrounds/fun-index)

## [Test Coverage](https://bagrounds.gitlab.io/fun-index/coverage/lcov-report/index.html)

## [API Docs](https://bagrounds.gitlab.io/fun-index/docs/index.html)

## Dependencies

### Without Tests

![Dependencies](https://bagrounds.gitlab.io/fun-index/img/dependencies.svg)

### With Tests

![Test Dependencies](https://bagrounds.gitlab.io/fun-index/img/dependencies-test.svg)

